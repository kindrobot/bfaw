package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
)

type input struct {
	Function string `json:"function"`
	Arg1     string `json:"arg1"`
	Arg2     string `json:"arg2"`
}

type orchestratorRequest struct {
	Fn   string `json:"Fn"`
	Arg1 string `json:"Arg1"`
	Arg2 string `json:"Arg2"`
}

type orchestratorResponse struct {
	Result string `json:"Result"`
}

type output struct {
	Result string `json:"result"`
}

func main() {
	orchestratorHost := os.Getenv("ORCHESTRATOR_HOST")
	if orchestratorHost == "" {
		panic("Must provide ORCHESTRATOR_HOST")
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		var jsonReq input
		err := json.NewDecoder(r.Body).Decode(&jsonReq)
		if err != nil {
			panic(err)
		}
		orchestratorJson := orchestratorRequest{
			Fn:   jsonReq.Function,
			Arg1: jsonReq.Arg1,
			Arg2: jsonReq.Arg2,
		}
		orchestratorUri := "http://" + orchestratorHost
		buffer := new(bytes.Buffer)
		json.NewEncoder(buffer).Encode(orchestratorJson)
		orchResp, err := http.Post(orchestratorUri, "application/json", buffer)
		if err != nil {
			panic(err)
		}
		var orchResponseJson orchestratorResponse
		err = json.NewDecoder(orchResp.Body).Decode(&orchResponseJson)
		if err != nil {
			panic(err)
		}
		responseJson := output(orchResponseJson)
		json.NewEncoder(w).Encode(responseJson)
	})
	port := os.Getenv("PORT")
	if port == "" {
		port = "8000"
	}
	host := ":" + port
	fmt.Println("Listening on", host)
	err := http.ListenAndServe(host, nil)
	if err != nil {
		panic(err)
	}
}
