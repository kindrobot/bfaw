package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strings"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		expression := r.URL.Query().Get("expression")
		log.Println("received expression:", expression)
		result := execMath(expression)
		log.Println("returning result:", result)
		io.WriteString(w, result)
	})
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	host := ":" + port
	fmt.Println("Listening on", host)
	err := http.ListenAndServe(host, nil)
	if err != nil {
		panic(err)
	}
}

func execMath(expr string) (result string) {
	echo := exec.Command("echo", expr)
	bc := exec.Command("bc", "-l")
	pr, pw, err := os.Pipe()
	if err != nil {
		panic(err)
	}
	echo.Stdout = pw
	bc.Stdin = pr
	err = echo.Run()
	if err != nil {
		panic(err)
	}
	err = pw.Close()
	if err != nil {
		return
	}
	out, err := bc.Output()
	if err != nil {
		panic(err)
	} else {
		result = strings.TrimSpace(string(out))
	}
	return
}
