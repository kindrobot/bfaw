build:
  cd api; CGO_ENABLED=0 go build
  cd orchestrator; CGO_ENABLED=0 go build
  cd evaluator; CGO_ENABLED=0 go build

docker-build VERSION: build
  cd api; docker build . -t ku001:32000/bfaw-api:{{VERSION}}
  cd orchestrator; docker build . -t ku001:32000/bfaw-orchestrator:{{VERSION}}
  cd evaluator; docker build . -t ku001:32000/bfaw-evaluator:{{VERSION}}

docker-push VERSION: (docker-build VERSION)
  docker push ku001:32000/bfaw-api:{{VERSION}}
  docker push ku001:32000/bfaw-orchestrator:{{VERSION}}
  docker push ku001:32000/bfaw-evaluator:{{VERSION}}
