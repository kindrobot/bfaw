package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"strings"
)

type fnMap map[string]string

type input struct {
	Fn, Arg1, Arg2 string
}

type output struct {
	Result string
}

func main() {
	if len(os.Args) != 2 {
		panic("Must supply path to function map")
	}
	evaluatorHost := os.Getenv("EVALUATOR_HOST")
	if evaluatorHost == "" {
		panic("Must provide EVALUATOR_HOST")
	}
	knownFns := getFnMap(os.Args[1])

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		var jsonReq input
		err := json.NewDecoder(r.Body).Decode(&jsonReq)
		if err != nil {
			panic(err)
		}
		opperator := knownFns[jsonReq.Fn]
		if opperator == "" {
			panic("unkown function")
		}
		expression := jsonReq.Arg1 + url.QueryEscape(opperator) + jsonReq.Arg2
		evaluatorUri := "http://" + evaluatorHost + "?expression=" + expression
		evalResp, err := http.Get(evaluatorUri)
		if err != nil {
			panic(err)
		}
		evalResult, err := io.ReadAll(evalResp.Body)
		if err != nil {
			panic(err)
		}
		jsonResponse := output{
			Result: string(evalResult),
		}
		json.NewEncoder(w).Encode(jsonResponse)
	})
	port := os.Getenv("PORT")
	if port == "" {
		port = "8001"
	}
	host := ":" + port
	fmt.Println("Listening on", host)
	err := http.ListenAndServe(host, nil)
	if err != nil {
		panic(err)
	}
}

func getFnMap(functionsFile string) (foundFns fnMap) {
	foundFns = make(fnMap)
	file, err := os.Open(functionsFile)
	if err != nil {
		panic(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		parts := strings.Fields(line)
		if len(parts) != 2 {
			panic("Each line should have only two words")
		}
		foundFns[parts[0]] = parts[1]
	}
	return
}
